"use strict";

/**
 * URL からページの情報を取得するクラス
 *
 * @param {string} url
 */
class PageInfo {
    constructor(url) {
        this.url = new URL(url);

        if (this.url.origin !== 'https://dic.nicovideo.jp') {
            throw new TypeError('Argument is not Nicodic URL.');
        }
    }

    get articleName() {
        const name = /^\/[alviu]\/(.+)/.exec(this.articlePath)[1];
        return decodeURIComponent(name);
    }

    get articlePath() {
        /* /t と /b を除いたパス名
         *   /a: 単語記事
         *   /l: 生放送記事
         *   /v: 動画記事
         *   /i: 商品記事
         *   /u: ユーザー記事
         *   (/c: コミュニティ記事は掲示板がないため除外)
         */
        return /^(\/t)?(\/b)?(\/[alviu]\/[^/]+)/.exec(this.url.pathname)[3];
    }

    get isBBS() {
        // /b もしくは /t/b から始まっているのは掲示板
        return /^(\/t)?\/b\//.test(this.url.pathname);
    }

    get isDesktop() {
        return !this.isMobile;
    }

    get isMobile() {
        // /t から始まっているのはスマホ版
        return /^\/t\//.test(this.url.pathname);
    }
}

/**
 * NGList (NGID, NGWord, etc...) を管理するクラス
 *
 * @param {string} key
 */
class NGList {
    constructor(key) {
        this.key = key;
    }

    async add(value) {
        const array = await this.get();
        if (!array.includes(value)) {
            array.push(value);
        }
        await this.set(array);
        return array;
    }

    async clear() {
        return await browser.storage.local.remove(this.key);
    }

    async get() {
        const hash = await browser.storage.local.get(this.key);
        return hash[this.key] || [];
    }

    async onChanged(listener) {
        const wrapper = (changes, areaName) => {
            const change = changes[this.key];
            if (!change) {
                // 無関係な変更は無視する
                return;
            }
            listener(change, areaName);
        };
        return browser.storage.onChanged.addListener(wrapper);
    }

    async remove(value) {
        const list = await this.get();
        const array = list.filter(e => e !== value);
        await this.set(array);
        return array;
    }

    async set(array) {
        return await browser.storage.local.set({
            [this.key]: array
        });
    }
}

/**
 * 掲示板の NG 操作を抽象化するクラス
 * DOM 操作は DesktopComment / MobileComment に分離
 */
class BoardComment {
    constructor() {
        // 初期状態ではあぼーんされていない
        this.aboned = false;
    }

    get body() { return ""; }
    set body(value) { value; }
    get button() { return ""; }
    set button(value) { value; }

    get deleted() {
        // Desktop: '    \n    削除しました\n    ID: XXXXXXXXX'
        // Mobile:  '削除しました ID: XXXXXXXXXX'
        return /^\s*削除しました\s+ID:/.test(this.info);
    }

    get id() {
        // Desktop: '    \n    1970/01/01(木) 00:00:00\n    ID: XXXXXXXXXX\n'
        // Mobile:  '1970/01/01(木) 00:00:00 ID: XXXXXXXXXX'
        return /\s+ID: ([^\s]+)\s*$/.exec(this.info)[1];
    }

    get info() { return ""; }

    get name() {
        return this.nameElement().textContent;
    }

    set name(value) {
        this.nameElement().textContent = value;
    }

    get number() { return ""; }
    get text() { return ""; }

    abone() {
        this.aboned = true;
        this.updateButton();

        if (this.deleted) {
            // 削除済みなら何もしない
            return;
        }

        if (!this.original) {
            // 最初の一回だけコピーする
            this.original = {
                name: this.name,
                body: this.body,
            }
        }

        const aboned_body = this.body.cloneNode(true);
        aboned_body.textContent = 'あぼーん';

        this.name = 'あぼーん';
        this.body = aboned_body;
    }

    addButton(nglist) {
        const button = document.createElement('span');
        button.setAttribute('class', 'abone-button');
        this.updateButton(button);

        const err = (e) => {
            console.error(e);
        }

        button.addEventListener('click', () => {
            if (!this.aboned) {
                nglist.add(this.id).catch(err);
            } else {
                nglist.remove(this.id).catch(err);
            }
        });

        this.button = button;
    }

    ngid(array) {
        const included = array.includes(this.id);

        if (!this.aboned && included) {
            this.abone();
        } else if (this.aboned && !included) {
            this.restore();
        }
    }

    nameElement() {}

    restore() {
        this.aboned = false;
        this.updateButton();

        if (this.deleted) {
            // 削除済みなら何もしない
            return;
        }

        this.name = this.original.name;
        this.body = this.original.body;
    }

    updateButton(element) {
        const button = element || this.button;

        if (!button) {
            // this.button が存在しない場合は何もしない
            return;
        }

        if (!this.aboned) {
            const block = browser.i18n.getMessage("blocking");
            button.textContent = `[${block}]`;
        } else {
            const unblock = browser.i18n.getMessage("unblocking");
            button.textContent = `[${unblock}]`;
        }
    }
}

/**
 * デスクトップ版の掲示板を扱うクラス
 *
 * @param {HTMLElement} header
 */
class DesktopComment extends BoardComment {
    constructor(header) {
        super();
        this.header = header;
    }

    get body() {
        return this.header.nextElementSibling;
    }

    set body(value) {
        const body = this.body;
        body.parentNode.replaceChild(value, body);
    }

    get button() {
        return this.header.querySelector(".abone-button");
    }

    set button(value) {
        if (this.button) {
            // ボタンが存在するときは新しいものに更新する
            this.header.replaceChild(value, this.button);
        } else {
            // ボタンが存在しないときは追加する
            this.header.appendChild(value);
        }
    }

    get info() {
        return this.header.querySelector(".st-bbs_resInfo").textContent;
    }

    get number() {
        return this.header.querySelector(".st-bbs_resNo").textContent;
    }

    get text() {
        return this.body.textContent;
    }

    nameElement() {
        return this.header.querySelector(".st-bbs_name");
    }
}

/**
 * スマホ版の掲示板を扱うクラス
 *
 * @param {HTMLElement} parent
 */
class MobileComment extends BoardComment {
    constructor(parent) {
        super();
        this.parent = parent;
    }

    get body() {
        return this.parent.querySelector(".at-List_Text");
    }

    set body(value) {
        const body = this.body;
        this.parent.replaceChild(value, body);
    }

    get button() {
        return this.parent.querySelector(".abone-button");
    }

    set button(value) {
        const container = this.parent.querySelector(".at-List_Date");
        if (this.button) {
            // ボタンが存在するときは新しいものに更新する
            container.replaceChild(value, this.button);
        } else {
            // ボタンが存在しないときは追加する
            container.appendChild(value);
        }
    }

    get info() {
        return this.parent.querySelector(".at-List_Date").firstChild.textContent;
    }

    get number() {
        return this.parent.querySelector(".at-List_Name > .at-List_Num").textContent;
    }

    get text() {
        return this.parent.querySelector(".at-List_Text").textContent;
    }

    nameElement() {
        return this.parent.querySelector(".at-List_Name").lastChild;
    }
}

const getComments = (isDesktop) => {
    if (isDesktop) {
        return Array.from(
            document.querySelectorAll(".st-bbs-contents > dl > dt.st-bbs_reshead")
        );
    } else {
        return Array.from(
            document.querySelectorAll("ul.sw-Article_List > li")
        );
    }
}

const page = new PageInfo(location.href);
const list = getComments(page.isDesktop);
const comments = list.map(e => {
    if (page.isDesktop) {
        return new DesktopComment(e);
    } else {
        return new MobileComment(e);
    }
});

const ngid = new NGList(page.articlePath);
ngid.onChanged((change) => {
    const array = change.newValue || [];
    comments.forEach((c) => c.ngid(array));
}).catch((err) => {
    console.error(err);
});

ngid.get()
    .then((array) => comments.forEach((c) => c.ngid(array)))
    .catch((err) => console.error(err));

comments.forEach(c => {
    c.addButton(ngid);
});
